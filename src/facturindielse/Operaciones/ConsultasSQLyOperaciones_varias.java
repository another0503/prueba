/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facturindielse.Operaciones;

import facturindielse.ConexionBD;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author condo
 */
public class ConsultasSQLyOperaciones_varias {

    ConexionBD c = new ConexionBD();
    Connection con = c.conecxion();
    
    
    
    /*******************************
     *  TODO LO RELACIONADO CON SQL*
     *******************************/
    public  ArrayList ConsultaTabla(int numCamposTabla, String instruccion, String errorMsg) {
        Statement orden = null;
        ResultSet r;
        ArrayList<Object[]> datos = new ArrayList<Object[]>();
            try {
                //prepararTabla();
                orden = con.createStatement();
                    r = orden.executeQuery(instruccion);
                    
               
                while(r.next()){
                    Object filas[] = new Object[numCamposTabla];
                    //para determinar el numero de campos de la consulta
                    for(int i=0;i<numCamposTabla;i++){
                        filas[i]=r.getString(i+1);
                    }

                    datos.add(filas);
                    
                }

            } catch (Exception e) {
                //Logger.getLogger(Clientes.class.getName()).log(Level.SEVERE, null, e);
                 JOptionPane.showMessageDialog(null, errorMsg+" "+e);
            }
            return datos;
    }
    
    public boolean OperacionSQL(int numParametros, ArrayList<String> parametros, String instruccion, String errorMsg, String msg) {
        ResultSet r;
        try {
            PreparedStatement op = con.prepareStatement(instruccion);
            for (int i = 0; i < parametros.size(); i++) {
                op.setString(i+1, parametros.get(i));
            }

            int state = op.executeUpdate();

            if (state > 0) {
                JOptionPane.showMessageDialog(null, msg);
                return true;
            }else{
                JOptionPane.showMessageDialog(null, errorMsg);
                return false;
            }

        } catch (Exception e) {
            //Logger.getLogger(Clientes.class.getName()).log(Level.SEVERE, null, e);
            JOptionPane.showMessageDialog(null, errorMsg + " " + e);
            return false;
        }

    }
    
    public DefaultTableModel pintaTabla(DefaultTableModel mod, ArrayList<Object[]> datos){
        
        for (int i = 0; i < datos.size(); i++) {
            mod.addRow(datos.get(i));
        }
        
        return mod;
    }
    
    
    
    
    /************************************
     *  TODO LO RELACIONADO CON ventanas*
     ************************************/
    
    public int Confirmar(String msg) {

        int eleccion = JOptionPane.showOptionDialog(null,
                msg,
                "Confirmar",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null, null, null);

        return eleccion;
    }
}
