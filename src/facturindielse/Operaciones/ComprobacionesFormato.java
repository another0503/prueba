/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facturindielse.Operaciones;


import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author condo
 */
public class ComprobacionesFormato {
    
    
    
    
    static public boolean noVacio(JTextField[] a){
        boolean valido = true;
        
        for(int i=0; i<a.length;i++){
            if(a[i].getText().equals("")){
                valido=false;
            }
        }
        
        return valido;
    }
    
    public static DefaultTableModel prepararTabla(String titulos[],DefaultTableModel mod, JTable tabla){
        mod = new DefaultTableModel(null,titulos);
        tabla.setModel(mod);
       
        return mod;
    }
}
